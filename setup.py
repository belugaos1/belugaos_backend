from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    """
        Dummy code to initialize proyect
    """
    return 'Hello, World!'